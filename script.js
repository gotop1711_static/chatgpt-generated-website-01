// Code to handle user interactions

/* Homepage Tab-and-Content Group Component - JavaScript */
// Get all the tab content elements and hide them by default
const tabContents = document.querySelectorAll(".tabcontent");
tabContents.forEach(content => content.style.display = "none");

// Get all the tab buttons and add event listeners to them
const tabButtons = document.querySelectorAll(".tablinks");
tabButtons.forEach(button => {
  button.addEventListener("click", function () {
    // Remove the active class from all buttons
    tabButtons.forEach(button => button.classList.remove("active"));

    // Hide all the tab content
    tabContents.forEach(content => content.style.display = "none");

    // Show the clicked tab content and mark the button as active
    const tabContentId = this.getAttribute("data-tabcontent");
    const tabContent = document.getElementById(tabContentId);
    tabContent.style.display = "block";
    this.classList.add("active");
  });
});
