; (function () {
  // DOMContentLoaded
  document.addEventListener(
    'DOMContentLoaded',
    init
  )

  function init() {
    const tablinks = document.querySelectorAll('.tablinks');
    const tabcontent = document.querySelectorAll('.tabcontent');

    registerTabsEvents()

    function registerTabsEvents() {
      console.log('registerTabsEvents');

      tablinks.forEach(function (tab, tabIndex) {
        tab.addEventListener('click', function (e) {
          console.log('clicked');

          resetTabs()
          e.currentTarget.classList.add('active')
          tabcontent[tabIndex].classList.add('active')
        })
      })
    }

    function resetTabs() {
      console.log('resetTabs');

      tablinks.forEach(function (tab, tabIndex) {
        tab.classList.remove('active');
        tabcontent[tabIndex].classList.remove('active');
      })
    }
  }


}())
